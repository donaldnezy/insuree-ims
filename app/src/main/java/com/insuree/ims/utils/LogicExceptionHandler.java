package com.insuree.ims.utils;

import android.app.Activity;
import android.content.Intent;

import com.insuree.ims.activity.LoginActivity;

/**
 * Created by Millo on 12/20/2017.
 */

public class LogicExceptionHandler implements Thread.UncaughtExceptionHandler {

    public static final String EXTRA_MY_EXCEPTION_HANDLER = "EXCEPTION_HANDLER";
    private final Activity context;
    private final Thread.UncaughtExceptionHandler rootHandler;

    public LogicExceptionHandler(Activity context) {
        this.context = context;
        rootHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    @Override
    public void uncaughtException(final Thread thread, final Throwable ex) {
        Intent registerActivity = new Intent(context, LoginActivity.class);
        registerActivity.putExtra(EXTRA_MY_EXCEPTION_HANDLER, LogicExceptionHandler.class.getName());
        registerActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        registerActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);

        context.startActivity(registerActivity);

        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(0);
    }
}