package com.insuree.ims.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.insuree.ims.R;
import com.insuree.ims.databinding.MainDataBinder;
import com.insuree.ims.fragment.MenuFragment;
import com.insuree.ims.fragment.NewItemFragment;
import com.insuree.ims.fragment.ProfileFragment;
import com.insuree.ims.fragment.SettingsFragment;
import com.insuree.ims.model.UserModel;
import com.insuree.ims.viewmodel.UserViewModel;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_main);

        mDatabase = FirebaseDatabase.getInstance().getReference("users");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final MainDataBinder mainDataBinder = DataBindingUtil.bind(navigationView.getHeaderView(0));
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            String userId = firebaseUser.getUid();
            mDatabase.child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserModel user = dataSnapshot.getValue(UserModel.class);
                    mainDataBinder.setUser(new UserViewModel(user));
                    if (user.getImgUrl() != null && !TextUtils.isEmpty(user.getImgUrl()))
                    Glide.with(MainActivity.this).load(user.getImgUrl())
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into((ImageView) mainDataBinder.getRoot().findViewById(R.id.imageView));
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                }
            });
        }

        initView();
    }

    private void initView() {
        MenuFragment menuFragment = new MenuFragment();
        String tag = menuFragment.getClass().getName();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.login_frame_content, menuFragment, tag)
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_menu) {
            showFragment(new MenuFragment());
        } else if (id == R.id.nav_create_item) {
            showFragment(new NewItemFragment());
        } else if (id == R.id.nav_profile) {
            showFragment(new ProfileFragment());
        } else if (id == R.id.nav_settings) {
            showFragment(new SettingsFragment());
        } else if (id == R.id.nav_about) {
            showAboutDialog();
        } else if (id == R.id.nav_logout) {
            //do logout
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showAboutDialog(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(R.string.str_company_name)
                .setMessage(R.string.str_poc_about)
                .setPositiveButton(R.string.str_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void showFragment(Fragment fragment) {
        String tag = fragment.getClass().getName();
        Fragment fr = getSupportFragmentManager().findFragmentByTag(tag);
        if (fr != null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_frame_content, fr, tag)
                    .commit();
        else {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_frame_content, fragment, tag)
                    .commit();
        }
    }
}
