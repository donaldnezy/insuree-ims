package com.insuree.ims.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.insuree.ims.R;
import com.insuree.ims.fragment.LoginFragment;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mAuth = FirebaseAuth.getInstance();
        if (mAuth.getCurrentUser() == null) {
            LoginFragment loginFragment = new LoginFragment();
            String tag = loginFragment.getClass().getName();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.login_frame_content, loginFragment, tag)
                    .commit();
        }else{
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }
}
