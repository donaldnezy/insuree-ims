package com.insuree.ims.event;

/**
 * Created by Millo on 12/21/2017.
 */

public interface ISettingsViewListener {
    void onMenuLayoutChange();
}
