package com.insuree.ims.event;

/**
 * Created by Millo on 12/20/2017.
 */

public interface IProfileViewListener {
    void onSelectImageClick();
    void onUpdateClick();
}
