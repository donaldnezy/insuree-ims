package com.insuree.ims.event;

import com.insuree.ims.viewmodel.ItemViewModel;

/**
 * Created by Millo on 12/20/2017.
 */

public interface IItemAdapterListener {
    void onItemClick(ItemViewModel itemViewModel);
}
