package com.insuree.ims.event;

/**
 * Created by Millo on 12/19/2017.
 */

public interface ILoginViewListener {
    void onClickLogin();
    void onClickRegisterNew();
}
