package com.insuree.ims.model;

import com.google.firebase.database.IgnoreExtraProperties;
import com.insuree.ims.viewmodel.ItemViewModel;

/**
 * Created by Millo on 12/20/2017.
 */

@IgnoreExtraProperties
public class ItemModel {
    private String id;
    private String name;
    private String price;
    private String imgUrl;
    private String count;

    public ItemModel() {
    }

    public ItemModel(String id, String name, String price, String imgUrl, String count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.imgUrl = imgUrl;
        this.count = count;
    }

    public ItemModel(String id, ItemViewModel viewModel) {
        this.id = id;
        this.name = viewModel.getName();
        this.price = viewModel.getPrice();
        this.imgUrl = viewModel.getImgUrl();
        this.count = viewModel.getCount();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
