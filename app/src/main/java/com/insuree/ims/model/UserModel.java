package com.insuree.ims.model;

import com.google.firebase.database.IgnoreExtraProperties;
import com.insuree.ims.viewmodel.UserViewModel;

/**
 * Created by Millo on 12/20/2017.
 */

@IgnoreExtraProperties
public class UserModel {
    private String id;
    private String firstName;
    private String otherNames;
    private String email;
    private String password;
    private String imgUrl;

    public UserModel() {
    }

    public UserModel(String id, String firstName, String otherNames, String email, String password, String imgUrl) {
        this.id = id;
        this.firstName = firstName;
        this.otherNames = otherNames;
        this.email = email;
        this.password = password;
        this.imgUrl = imgUrl;
    }

    public UserModel(String id, UserViewModel viewModel) {
        this.id = id;
        this.firstName = viewModel.getFirstName();
        this.otherNames = viewModel.getOtherNames();
        this.email = viewModel.getEmail();
        this.password = viewModel.getPassword();
        this.imgUrl = viewModel.getImgUrl();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
