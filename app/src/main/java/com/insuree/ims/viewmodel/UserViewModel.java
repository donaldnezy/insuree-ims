package com.insuree.ims.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import com.insuree.ims.R;
import com.insuree.ims.model.UserModel;

/**
 * Created by Millo on 12/18/2017.
 */

public class UserViewModel extends BaseObservable {
    private String firstName;
    private String otherNames;
    private String email;
    private String password;
    private String imgUrl;
    //
    private String btnText;
    private String firstNameHint;
    private String otherNamesHint;
    private String emailHint;
    private String passwordHint;

    private Uri filePath;

    public UserViewModel(String btnText, String firstNameHint, String otherNamesHint, String emailHint, String passwordHint) {
        this.btnText = btnText;
        this.firstNameHint = firstNameHint;
        this.otherNamesHint = otherNamesHint;
        this.emailHint = emailHint;
        this.passwordHint = passwordHint;
    }

    public UserViewModel(UserModel userModel) {
        this.firstName = userModel.getFirstName();
        this.otherNames = userModel.getOtherNames();
        this.email = userModel.getEmail();
        this.password = userModel.getPassword();
        this.imgUrl = userModel.getImgUrl();
    }

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    public String getFirstNameHint() {
        return firstNameHint;
    }

    public void setFirstNameHint(String firstNameHint) {
        this.firstNameHint = firstNameHint;
    }

    public String getOtherNamesHint() {
        return otherNamesHint;
    }

    public void setOtherNamesHint(String otherNamesHint) {
        this.otherNamesHint = otherNamesHint;
    }

    public String getEmailHint() {
        return emailHint;
    }

    public void setEmailHint(String emailHint) {
        this.emailHint = emailHint;
    }

    public String getPasswordHint() {
        return passwordHint;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint;
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }

    @Bindable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        notifyPropertyChanged(R.id.edtFirstName);
    }

    @Bindable
    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
        notifyPropertyChanged(R.id.edtOtherNames);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(R.id.edtEmail);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(R.id.edtPassword);
    }

    @Bindable
    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
