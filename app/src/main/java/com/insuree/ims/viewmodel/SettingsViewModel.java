package com.insuree.ims.viewmodel;

/**
 * Created by Millo on 12/20/2017.
 */

public class SettingsViewModel {
    private boolean menuLayoutTypeGrid;

    public SettingsViewModel(boolean menuLayoutTypeGrid) {
        this.menuLayoutTypeGrid = menuLayoutTypeGrid;
    }

    public boolean isMenuLayoutTypeGrid() {
        return menuLayoutTypeGrid;
    }

    public void setMenuLayoutTypeGrid(boolean menuLayoutTypeGrid) {
        this.menuLayoutTypeGrid = menuLayoutTypeGrid;
    }
}
