package com.insuree.ims.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;

import com.insuree.ims.BR;
import com.insuree.ims.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Millo on 12/18/2017.
 */

public class LoginViewModel extends BaseObservable {
    private String email;
    private String password;
    private String emailHint;
    private String passwordHint;
    private String loginBtnText;
    private String registerNewText;

    public LoginViewModel(String emailHint, String passwordHint, String loginBtnText, String registerNewText) {
        this.emailHint = emailHint;
        this.passwordHint = passwordHint;
        this.loginBtnText = loginBtnText;
        this.registerNewText = registerNewText;
    }

    public String getEmailHint() {
        return emailHint;
    }

    public void setEmailHint(String emailHint) {
        this.emailHint = emailHint;
    }

    public String getPasswordHint() {
        return passwordHint;
    }

    public void setPasswordHint(String passwordHint) {
        this.passwordHint = passwordHint;
    }

    public String getLoginBtnText() {
        return loginBtnText;
    }

    public void setLoginBtnText(String loginBtnText) {
        this.loginBtnText = loginBtnText;
    }

    public String getRegisterNewText() {
        return registerNewText;
    }

    public void setRegisterNewText(String registerNewText) {
        this.registerNewText = registerNewText;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(R.id.edtEmail);
        notifyPropertyChanged(BR.validateEmailMessage);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(R.id.edtPassword);
    }

    @Bindable
    public String getValidateEmailMessage() {
        if(TextUtils.isEmpty(email)) {
            return "Enter your email address";
        } else if(!isValidEmail(email)) {
            return "Enter a valid email address";
        } else {
            return null;
        }
    }

    @Bindable
    public String getValidatePasswordMessage() {
        if(TextUtils.isEmpty(password)) {
            return "Enter your password";
        } else if(password.length() < 5) {
            return "Password is too short";
        } else {
            return null;
        }
    }

    public static boolean isValidEmail(String userEmail) {
        Pattern pattern;
        Matcher matcher;
        pattern= android.util.Patterns.EMAIL_ADDRESS;
        matcher = pattern.matcher(userEmail);
        return matcher.matches();
    }
}
