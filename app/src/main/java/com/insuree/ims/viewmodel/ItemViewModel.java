package com.insuree.ims.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.net.Uri;

import com.insuree.ims.R;
import com.insuree.ims.model.ItemModel;

import java.io.Serializable;

/**
 * Created by Millo on 12/19/2017.
 */

public class ItemViewModel extends BaseObservable implements Serializable {
    private String id;
    private String name;
    private String price;
    private String imgUrl;
    private String count;

    private String nameHint;
    private String priceHint;
    private String countHint;

    private String purpose;
    private String btnText;

    private Uri filePath;

    public ItemViewModel() {
    }

    public ItemViewModel(String nameHint, String priceHint, String countHint, String purpose, String btnText) {
        this.nameHint = nameHint;
        this.priceHint = priceHint;
        this.countHint = countHint;
        this.purpose = purpose;
        this.btnText = btnText;
    }

    public ItemViewModel(ItemModel itemModel) {
        this.id = itemModel.getId();
        this.name = itemModel.getName();
        this.price = itemModel.getPrice();
        this.imgUrl = itemModel.getImgUrl();
        this.count = itemModel.getCount();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Uri getFilePath() {
        return filePath;
    }

    public void setFilePath(Uri filePath) {
        this.filePath = filePath;
    }

    public String getNameHint() {
        return nameHint;
    }

    public void setNameHint(String nameHint) {
        this.nameHint = nameHint;
    }

    public String getPriceHint() {
        return priceHint;
    }

    public void setPriceHint(String priceHint) {
        this.priceHint = priceHint;
    }

    public String getCountHint() {
        return countHint;
    }

    public void setCountHint(String countHint) {
        this.countHint = countHint;
    }

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(R.id.edtEmail);
    }

    @Bindable
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    @Bindable
    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }
}
