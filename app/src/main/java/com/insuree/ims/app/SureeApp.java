package com.insuree.ims.app;

import android.app.Application;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Millo on 12/20/2017.
 */

public class SureeApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}
