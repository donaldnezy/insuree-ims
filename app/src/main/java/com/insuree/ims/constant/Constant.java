package com.insuree.ims.constant;

/**
 * Created by Millo on 12/21/2017.
 */

public class Constant {
    public static final String DB_ITEMS = "items";
    public static final String ITEM = "item";
    public static final String DB_IMAGE_PATH = "images/";
    public static final String DB_USERS = "users";
    public static final int PICK_IMAGE_REQUEST = 71;
}
