package com.insuree.ims.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.insuree.ims.R;
import com.insuree.ims.activity.MainActivity;
import com.insuree.ims.constant.Constant;
import com.insuree.ims.databinding.ItemDataBinder;
import com.insuree.ims.event.IItemViewListener;
import com.insuree.ims.model.ItemModel;
import com.insuree.ims.viewmodel.ItemViewModel;

import java.io.IOException;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class NewItemFragment extends BaseFragment {

    private ItemDataBinder mItemDataBinder;
    private Uri mFilePath;

    private StorageReference mStorageReference;
    private DatabaseReference mDatabase;

    private ItemViewModel mItemViewModel;

    public NewItemFragment() {
        // Required empty public constructor
    }

    public static NewItemFragment newInstance(ItemViewModel item) {
        NewItemFragment fragment = new NewItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(Constant.ITEM, item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemViewModel = (ItemViewModel) getArguments().getSerializable(Constant.ITEM);
        }
        mStorageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constant.DB_ITEMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mItemDataBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_new_item, container, false);
        if (isCreatingNew()) {
            ItemViewModel viewModel = new ItemViewModel(getString(R.string.str_name), getString(R.string.str_price), getString(R.string.str_count), getString(R.string.str_new_item), getString(R.string.str_create));
            mItemDataBinder.setItem(viewModel);
        } else {
            mItemViewModel.setNameHint(getString(R.string.str_name));
            mItemViewModel.setPriceHint(getString(R.string.str_price));
            mItemViewModel.setCountHint(getString(R.string.str_count));
            mItemViewModel.setPurpose(getString(R.string.str_update_item));
            mItemViewModel.setBtnText(getString(R.string.str_update));

            mItemDataBinder.setItem(mItemViewModel);

            if (!TextUtils.isEmpty(mItemViewModel.getImgUrl()))
                Glide.with(getContext()).load(mItemViewModel.getImgUrl())
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into((ImageView) mItemDataBinder.getRoot().findViewById(R.id.imgItem));

            mItemDataBinder.btnDelete.setVisibility(View.VISIBLE);
        }
        mItemDataBinder.setListener(new IItemViewListener() {
            @Override
            public void onSelectImageClick() {
                browseImage();
            }

            @Override
            public void onDoneClick() {
                createOrUpdateItem();
            }

            @Override
            public void onDeleteClick() {
                if (mItemViewModel != null) {
                    mDatabase.child(mItemViewModel.getId()).removeValue();
                    getActivity().onBackPressed();
                }
            }
        });

        return mItemDataBinder.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                mFilePath = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mFilePath);

                    ItemViewModel itemViewModel = mItemDataBinder.getItem();
                    itemViewModel.setFilePath(mFilePath);
                    mItemDataBinder.imgItem.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void createOrUpdateItem() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        if (mFilePath != null) {
            progressDialog.setTitle(getString(R.string.str_processing));
            progressDialog.show();

            StorageReference ref = mStorageReference.child(Constant.DB_IMAGE_PATH + UUID.randomUUID().toString());
            ref.putFile(mFilePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();

                            ItemViewModel viewModel = mItemDataBinder.getItem();

                            String msg;
                            String itemId;
                            if (isCreatingNew()) {
                                itemId = mDatabase.push().getKey();
                                msg = getString(R.string.str_create_sucess);
                            } else {
                                itemId = mItemViewModel.getId();
                                msg = getString(R.string.str_update_success);
                            }
                            ItemModel itemModel = new ItemModel(itemId, viewModel);
                            itemModel.setImgUrl(String.valueOf(taskSnapshot.getDownloadUrl()));
                            mDatabase.child(itemId).setValue(itemModel);
                            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();

                            mFilePath = null;
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), getString(R.string.str_failed) + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage(getString(R.string.processing) + (int) progress + "%");
                        }
                    });
        } else {
            ItemViewModel viewModel = mItemDataBinder.getItem();
            String msg;
            String itemId;
            if (isCreatingNew()) {
                itemId = mDatabase.push().getKey();
                msg = getString(R.string.str_create_sucess);
            } else {
                itemId = mItemViewModel.getId();
                msg = getString(R.string.str_update_success);
            }
            ItemModel itemModel = new ItemModel(itemId, viewModel);
            mDatabase.child(itemId).setValue(itemModel);
            progressDialog.dismiss();
            Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();

            if (isCreatingNew()) {
                ((MainActivity) getActivity()).showFragment(new MenuFragment());
            } else {
                getActivity().onBackPressed();
            }
        }
    }

    private boolean isCreatingNew() {
        return mItemViewModel == null;
    }
}
