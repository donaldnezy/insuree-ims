package com.insuree.ims.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.insuree.ims.R;
import com.insuree.ims.activity.MainActivity;
import com.insuree.ims.databinding.LoginDataBinder;
import com.insuree.ims.event.ILoginViewListener;
import com.insuree.ims.viewmodel.LoginViewModel;

public class LoginFragment extends BaseFragment {

    private FirebaseAuth mAuth;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final LoginDataBinder loginDataBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        LoginViewModel loginViewModel = new LoginViewModel(getString(R.string.str_email), getString(R.string.str_password), getString(R.string.str_login), getString(R.string.new_user));
        loginDataBinder.setLogin(loginViewModel);


        loginDataBinder.setListener(new ILoginViewListener() {
            @Override
            public void onClickLogin() {
                String email = loginDataBinder.getLogin().getEmail();
                String password = loginDataBinder.getLogin().getPassword();
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setTitle(getString(R.string.str_authenticating));
                progressDialog.show();
                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressDialog.dismiss();
                                if (task.isSuccessful()) {
                                    Intent intent = new Intent(getContext(), MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                } else {
                                    Toast.makeText(getContext(), R.string.str_login_invalid_detail, Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            }

            @Override
            public void onClickRegisterNew() {
                showFragment(new UserRegisterFragment());
            }
        });
        return loginDataBinder.getRoot();
    }
}
