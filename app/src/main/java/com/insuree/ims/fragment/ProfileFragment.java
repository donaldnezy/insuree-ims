package com.insuree.ims.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.insuree.ims.R;
import com.insuree.ims.constant.Constant;
import com.insuree.ims.databinding.ProfileDataBinder;
import com.insuree.ims.event.IProfileViewListener;
import com.insuree.ims.model.UserModel;
import com.insuree.ims.viewmodel.UserViewModel;

import java.io.IOException;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends BaseFragment {

    private ProfileDataBinder mProfileDataBinder;
    private Uri mFilePath;

    private StorageReference mStorageReference;
    private DatabaseReference mDatabase;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStorageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constant.DB_USERS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mProfileDataBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);

        mProfileDataBinder.setListener(new IProfileViewListener() {
            @Override
            public void onSelectImageClick() {
                browseImage();
            }

            @Override
            public void onUpdateClick() {
                updateProfile();
            }
        });


        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (firebaseUser != null) {
            String userId = firebaseUser.getUid();
            mDatabase.child(userId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    UserModel user = dataSnapshot.getValue(UserModel.class);
                    UserViewModel viewModel = new UserViewModel(user);
                    viewModel.setBtnText(getString(R.string.str_update));
                    viewModel.setFirstNameHint(getString(R.string.str_first_name));
                    viewModel.setOtherNamesHint(getString(R.string.str_other_names));
                    viewModel.setEmailHint(getString(R.string.str_email));
                    viewModel.setPasswordHint(getString(R.string.str_password));
                    mProfileDataBinder.setUser(viewModel);
                    if (user.getImgUrl() != null && !TextUtils.isEmpty(user.getImgUrl()))
                    Glide.with(getContext()).load(user.getImgUrl())
                            .crossFade()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .into((ImageView) mProfileDataBinder.getRoot().findViewById(R.id.imgProfilePic));
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                }
            });
        }

        return mProfileDataBinder.getRoot();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if (data != null && data.getData() != null) {
                mFilePath = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), mFilePath);

                    UserViewModel userViewModel = mProfileDataBinder.getUser();
                    userViewModel.setFilePath(mFilePath);
                    mProfileDataBinder.imgProfilePic.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateProfile() {
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        if (mFilePath != null) {
            progressDialog.setTitle(getString(R.string.str_updating_profile));
            progressDialog.show();

            StorageReference ref = mStorageReference.child(Constant.DB_IMAGE_PATH + UUID.randomUUID().toString());
            ref.putFile(mFilePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            UserViewModel userViewModel = mProfileDataBinder.getUser();

                            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                            if (firebaseUser != null) {
                                String userId = firebaseUser.getUid();
                                UserModel itemModel = new UserModel(userId, userViewModel);

                                itemModel.setImgUrl(String.valueOf(taskSnapshot.getDownloadUrl()));

                                mDatabase.child(userId).setValue(itemModel);

                                Toast.makeText(getContext(), getString(R.string.str_update_success), Toast.LENGTH_SHORT).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(getContext(), getString(R.string.str_failed) + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage(getString(R.string.updating) + (int) progress + "%");
                        }
                    });
        }else{
            UserViewModel userViewModel = mProfileDataBinder.getUser();

            FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            if (firebaseUser != null) {
                String userId = firebaseUser.getUid();
                UserModel userModel = new UserModel(userId, userViewModel);
                mDatabase.child(userId).setValue(userModel);

                progressDialog.dismiss();
                Toast.makeText(getContext(), getString(R.string.str_update_success), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
