package com.insuree.ims.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.insuree.ims.R;
import com.insuree.ims.databinding.SettingsViewBinder;
import com.insuree.ims.event.ISettingsViewListener;
import com.insuree.ims.utils.SharedPreferencesUtils;
import com.insuree.ims.viewmodel.SettingsViewModel;

public class SettingsFragment extends Fragment {

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SettingsViewBinder settingsViewBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);

        int menuLayoutType = SharedPreferencesUtils.getMenuLayoutType(getContext());
        final SettingsViewModel settingsViewModel = new SettingsViewModel(menuLayoutType != 0);

        settingsViewBinder.setSetting(settingsViewModel);
        settingsViewBinder.setListener(new ISettingsViewListener() {
            @Override
            public void onMenuLayoutChange() {
                int menuType = settingsViewModel.isMenuLayoutTypeGrid() ? 1 : 0;
                SharedPreferencesUtils.setMenuLayoutType(getContext(), menuType);
            }
        });
        return settingsViewBinder.getRoot();
    }
}
