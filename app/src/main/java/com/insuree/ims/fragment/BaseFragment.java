package com.insuree.ims.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.insuree.ims.R;
import com.insuree.ims.constant.Constant;
import com.insuree.ims.utils.LogicExceptionHandler;

/**
 * Created by Millo on 12/19/2017.
 */

public class BaseFragment extends Fragment{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LogicExceptionHandler(getActivity());
    }

    protected void browseImage() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Image"), Constant.PICK_IMAGE_REQUEST);
    }

    protected void showFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        String tag = fragment.getClass().getName();
        Fragment fr = fragmentManager.findFragmentByTag(tag);
        if (fr != null)
            fragmentManager.beginTransaction()
                    .replace(R.id.login_frame_content, fr, tag).addToBackStack(tag)
                    .commit();
        else {
            fragmentManager.beginTransaction()
                    .replace(R.id.login_frame_content, fragment, tag).addToBackStack(tag)
                    .commit();
        }
    }
}
