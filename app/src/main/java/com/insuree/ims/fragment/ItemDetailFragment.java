package com.insuree.ims.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.insuree.ims.R;
import com.insuree.ims.databinding.ItemDetailDataBinder;
import com.insuree.ims.event.IItemDetailViewListener;
import com.insuree.ims.viewmodel.ItemViewModel;

public class ItemDetailFragment extends BaseFragment {

    private ItemViewModel mItemViewModel;
    private ItemDetailDataBinder mItemDataBinder;

    public ItemDetailFragment() {
        // Required empty public constructor
    }

    public static ItemDetailFragment newInstance(ItemViewModel item) {
        ItemDetailFragment fragment = new ItemDetailFragment();
        Bundle args = new Bundle();
        args.putSerializable("item", item);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItemViewModel = (ItemViewModel) getArguments().getSerializable("item");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mItemDataBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_item_detail, container, false);
        mItemViewModel.setBtnText(getString(R.string.str_edit));
        mItemDataBinder.setItem(mItemViewModel);
        if (!TextUtils.isEmpty(mItemViewModel.getImgUrl()))
            Glide.with(getContext()).load(mItemViewModel.getImgUrl())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into((ImageView) mItemDataBinder.getRoot().findViewById(R.id.imgItem));

        mItemDataBinder.setListener(new IItemDetailViewListener() {
            @Override
            public void onEdit() {
                getFragmentManager().popBackStack();
                showFragment(NewItemFragment.newInstance(mItemViewModel));
            }
        });

        return mItemDataBinder.getRoot();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            mItemViewModel.setBtnText(getString(R.string.str_edit));
        }
    }
}
