package com.insuree.ims.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.insuree.ims.R;
import com.insuree.ims.adapter.ItemAdapter;
import com.insuree.ims.constant.Constant;
import com.insuree.ims.model.ItemModel;
import com.insuree.ims.utils.SharedPreferencesUtils;
import com.insuree.ims.viewmodel.ItemViewModel;

import java.util.ArrayList;
import java.util.List;


public class MenuFragment extends BaseFragment implements ItemAdapter.OnAdapterListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);

        final List<ItemViewModel> items = new ArrayList<>();

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference(Constant.DB_ITEMS);
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    ItemModel itemModel = postSnapshot.getValue(ItemModel.class);
                    items.add(new ItemViewModel(itemModel));
                }
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);

        int menuLayoutType = SharedPreferencesUtils.getMenuLayoutType(getContext());
        if (menuLayoutType == 0)
            mLayoutManager = new LinearLayoutManager(getContext());
        else mLayoutManager = new GridLayoutManager(getContext(), 2);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new ItemAdapter(getContext(), items, this);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    @Override
    public void onItemClick(ItemViewModel itemViewModel) {
        showFragment(ItemDetailFragment.newInstance(itemViewModel));
    }
}
