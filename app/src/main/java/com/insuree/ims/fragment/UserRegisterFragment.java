package com.insuree.ims.fragment;

import android.app.ProgressDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.insuree.ims.R;
import com.insuree.ims.constant.Constant;
import com.insuree.ims.databinding.UserDataBinder;
import com.insuree.ims.event.IRegisterViewListener;
import com.insuree.ims.model.UserModel;
import com.insuree.ims.viewmodel.LoginViewModel;
import com.insuree.ims.viewmodel.UserViewModel;

public class UserRegisterFragment extends BaseFragment {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    public UserRegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference(Constant.DB_USERS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final UserDataBinder userDataBinder = DataBindingUtil.inflate(inflater, R.layout.fragment_user_register, container, false);
        UserViewModel userViewModel = new UserViewModel(getString(R.string.str_register), getString(R.string.str_first_name), getString(R.string.str_other_names), getString(R.string.str_email), getString(R.string.str_password));
        userDataBinder.setUser(userViewModel);

        userDataBinder.setListener(new IRegisterViewListener() {
            @Override
            public void onClickRegister() {
                UserViewModel user = userDataBinder.getUser();
                if (!LoginViewModel.isValidEmail(user.getEmail())){
                    userDataBinder.edtPassword.setError(getString(R.string.str_invalid_email));
                    Toast.makeText(getContext(), getString(R.string.str_invalid_email), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (user.getPassword().length() < 6) {
                    userDataBinder.edtPassword.setError(getString(R.string.str_invalid_password_lenght));
                    Toast.makeText(getContext(), getString(R.string.str_invalid_password_lenght), Toast.LENGTH_SHORT).show();
                    return;
                }
                final ProgressDialog progressDialog = new ProgressDialog(getContext());
                progressDialog.setTitle(getString(R.string.str_processing));
                progressDialog.show();
                mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressDialog.dismiss();
                                if (task.isSuccessful()) {
                                    String userId = task.getResult().getUser().getUid();
                                    UserModel userModel = new UserModel(userId, userDataBinder.getUser());
                                    mDatabase.child(userId).setValue(userModel);
                                    Toast.makeText(getContext(), R.string.str_register_success, Toast.LENGTH_SHORT).show();
                                    getActivity().onBackPressed();
                                } else {
                                    Toast.makeText(getContext(), getString(R.string.str_failed)+task.getException(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        return userDataBinder.getRoot();
    }
}
