package com.insuree.ims.adapter;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.insuree.ims.BR;
import com.insuree.ims.R;
import com.insuree.ims.databinding.SingleItemViewBinder;
import com.insuree.ims.event.IItemAdapterListener;
import com.insuree.ims.viewmodel.ItemViewModel;

import java.util.List;

/**
 * Created by Millo on 12/19/2017.
 */

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ItemViewHolder> {

    private List<ItemViewModel> mItemViewModels;
    private Context mContext;
    private OnAdapterListener mAdapterListener;

    public ItemAdapter(Context context, List<ItemViewModel> itemViewModels, OnAdapterListener mAdapterListener) {
        mItemViewModels = itemViewModels;
        this.mContext = context;
        this.mAdapterListener = mAdapterListener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        SingleItemViewBinder binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_list_layout, parent, false);
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        final ItemViewModel itemViewModel = mItemViewModels.get(position);
        holder.bind(itemViewModel);
    }

    @Override
    public int getItemCount() {
        return mItemViewModels.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {

        private final SingleItemViewBinder mBinder;

        public ItemViewHolder(SingleItemViewBinder binder) {
            super(binder.getRoot());
            this.mBinder = binder;
        }

        public void bind(ItemViewModel viewModel) {
            mBinder.setVariable(BR.item, viewModel);
            mBinder.executePendingBindings();
            if (!TextUtils.isEmpty(viewModel.getImgUrl()))
                Glide.with(mContext).load(viewModel.getImgUrl())
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into((ImageView) mBinder.getRoot().findViewById(R.id.imgItem));

            mBinder.setListener(new IItemAdapterListener() {
                @Override
                public void onItemClick(ItemViewModel itemViewModel) {
                    mAdapterListener.onItemClick(itemViewModel);
                }
            });
        }
    }

    public interface OnAdapterListener {
        void onItemClick(ItemViewModel itemViewModel);
    }
}